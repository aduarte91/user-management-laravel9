<?php

use Illuminate\Support\Facades\Route;
use Auth0\Laravel\Http\Controller\Stateful\Login;
use Auth0\Laravel\Http\Controller\Stateful\Logout;
use Auth0\Laravel\Http\Controller\Stateful\Callback;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/login', Login::class)->name('login');
Route::get('/logout', Logout::class)->name('logout');
Route::get('/auth0/callback', Callback::class)->name('auth0.callback');
Route::get('/profile', 'App\Http\Controllers\Auth\Auth0IndexController@profile' )->name( 'profile' )->middleware('auth');
