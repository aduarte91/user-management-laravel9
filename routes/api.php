<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/profile', function(Request $request){
    $auth = Auth::guard('app');
    $user = $auth->user();

    if(!$user->status){
        abort(403);
    }

    $accountInstanceUserTableName = 'account_instance_user';
    $accountInstanceTableName = 'account_instance';
    $organizationAccountTableName = 'organization_account';

    $accountInstance = DB::connection('app')
        ->table("{$accountInstanceUserTableName}")
        ->join("{$accountInstanceTableName}", "{$accountInstanceTableName}.id", '=', "{$accountInstanceUserTableName}.account_instance_id")
        ->join("{$organizationAccountTableName}", "{$organizationAccountTableName}.id", '=', "{$accountInstanceTableName}.organization_account_id")
        ->select(["{$organizationAccountTableName}.client_db_prefix", "$accountInstanceTableName.name", "$accountInstanceTableName.id"])
        ->first();

    return [
        'user_id' => $user->id,
        'nickname' => "{$user->first_name} {$user->last_name}",
        'email' => $user->email,
        'app_metadata'=> [
        'organization' => [
            'id' => $accountInstance->id,
            'prefix' => $accountInstance->client_db_prefix,
            'name' => $accountInstance->name,
            ]
        ],
    ];
})->middleware('auth.basic:app');
