# Installation
```
ln -s .env.dev .env
```

```
docker-compose up -d
```

```
docker exec -it um_app chown www-data:www-data storage/ bootstrap/ -R
```

```
docker exec -it um_app composer install
```
